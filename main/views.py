from rest_framework import viewsets
from main.models import ElementSalesNetwork
from main.serializers import ElementSalesNetworkSerializer, ElementSalesNetworkUpdateSerializer
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from main.filters import ElementSalesNetworkFilter
from main.permissions import IsActiveUser


class ElementSalesNetworkViewSet(viewsets.ModelViewSet):
    """
        Набор API (СЧИУ - Создание, Ч-чтение, И-изменение, У-удаление) для модели Звено сети продаж.
    """
    queryset = ElementSalesNetwork.objects.all()
    permission_classes = [IsAuthenticated, IsActiveUser]
    filter_backends = [DjangoFilterBackend]
    filterset_class = ElementSalesNetworkFilter

    def get_serializer_class(self):
        """
            Определяем сериализатор модели в зависимости вызванного метода http-запроса.
        """
        if self.action in ['update', 'partial_update']:
            return ElementSalesNetworkUpdateSerializer
        else:
            return ElementSalesNetworkSerializer
