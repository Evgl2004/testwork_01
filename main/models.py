from django.db import models
from django.utils import timezone

NULLABLE = {'null': True, 'blank': True}


class Contacts(models.Model):
    """
        Модель Контактов.

        Поля:
            - email: Электронная почта.
            - country: Наименование страны.
            - city: Наименование города.
            - street: Наименование улицы.
            - house_number: Номер дома.
        Методы:
            - __str__: Возвращает описание Контакта в виде строки.
        """
    email = models.EmailField(max_length=256, verbose_name='электронная почта')
    country = models.CharField(max_length=256, verbose_name='страна')
    city = models.CharField(max_length=256, verbose_name='город')
    street = models.CharField(max_length=256, verbose_name='улица')
    house_number = models.CharField(max_length=256, verbose_name='номер дома')

    def __str__(self):
        return f'{self.country} - {self.city} - {self.street} - {self.house_number}'

    class Meta:
        verbose_name = 'контакт'
        verbose_name_plural = 'контакты'


class Products(models.Model):
    """
        Модель Продуктов.

        Поля:
            - title: Название Продукта.
            - model: Наименование модели.
            - date_release: Дата выхода продукта на рынок.
        Методы:
            - __str__: Возвращает описание Продукта в виде строки.
        """
    title = models.CharField(max_length=256, verbose_name='название')
    model = models.CharField(max_length=256, verbose_name='модель')
    date_release = models.DateTimeField(default=timezone.now, verbose_name='дата релиза')

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'продукт'
        verbose_name_plural = 'продукты'


class ElementSalesNetwork(models.Model):
    """
        Модель звена/элемента сети продаж.

        Поля:
            - title: Название элемента сети продаж.
            - contact: Внешний ключ на модель Контакт.
            - product: Внешний ключ на модель Продукт.
            - provider: Поставщик, является элементом текущей модели Элемента сети продаж.
            - debt: Задолженность перед поставщиком в денежном выражении с точностью до копеек.
            - created_at: Время создания (заполняется автоматически при создании).
            - level_sales_network: Значение уровня иерархическую структуру сети продаж.
        Методы:
            - __str__: Возвращает описание элемента сети продаж в виде строки.
        """

    FACTORY = 0
    RETAIL = 1
    PRIVATE_BUSINESS = 2

    ITEM_TYPE = (
        (FACTORY, 'завод'),
        (RETAIL, 'розничная сеть'),
        (PRIVATE_BUSINESS, 'индивидуальный предприниматель'),
    )

    title = models.CharField(max_length=256, verbose_name='название')
    contact = models.ForeignKey(Contacts, on_delete=models.SET_NULL, **NULLABLE, verbose_name='контакт')
    product = models.ForeignKey(Products, on_delete=models.SET_NULL, **NULLABLE, verbose_name='продукты')
    provider = models.ForeignKey('self', on_delete=models.RESTRICT, **NULLABLE, verbose_name='поставщик')
    debt = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='задолженность')
    created_at = models.DateTimeField(default=timezone.now, verbose_name='дата создания')
    level_sales_network = models.IntegerField(default=PRIVATE_BUSINESS, choices=ITEM_TYPE, verbose_name='уровень')

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        """
            При сохранении экземпляра модели определяем уровень иерархической структуры.
        """
        if self.provider is None:
            self.level_sales_network = self.FACTORY
        else:
            self.level_sales_network = self.provider.level_sales_network + 1

        if self.level_sales_network > 2:
            self.level_sales_network = 2

        super(ElementSalesNetwork, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'элемент сети продаж'
        verbose_name_plural = 'элементы сети продаж'
