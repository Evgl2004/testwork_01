from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from main.models import Contacts, Products, ElementSalesNetwork


@admin.register(Contacts)
class ContactsAdmin(admin.ModelAdmin):
    list_display = ('id', 'country', 'city', 'street', 'house_number')


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


@admin.action(description='Очистить задолженность')
def make_clear_debt(modeladmin, request, queryset):
    queryset.update(debt=0)


@admin.register(ElementSalesNetwork)
class ElementSalesNetworkAdmin(admin.ModelAdmin):

    list_display = ('id', 'title', 'link_to_provider', 'level_sales_network', 'debt')
    list_filter = ('contact__city', )
    search_fields = ('contact__city', )

    actions = [make_clear_debt]

    def link_to_provider(self, obj):
        if obj.provider is None:
            return 'None'
        else:
            link = reverse("admin:main_elementsalesnetwork_change", args=[obj.provider.pk])
            return format_html('<a href="{}">{}</a>', link, obj.provider)
    link_to_provider.short_description = 'поставщик'
