from main.apps import MainConfig
from rest_framework.routers import DefaultRouter
from django.urls import include, path
from main.views import ElementSalesNetworkViewSet

app_name = MainConfig.name

router_esn = DefaultRouter()
router_esn.register(r'esn', ElementSalesNetworkViewSet, basename='element_sales_network')


urlpatterns = [
    path("", include(router_esn.urls)),
]
