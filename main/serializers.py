from rest_framework import serializers
from main.models import ElementSalesNetwork


class ElementSalesNetworkSerializer(serializers.ModelSerializer):
    """
        Сериализатор для экземпляра модели Звено сети продаж.
    """

    class Meta:
        model = ElementSalesNetwork
        fields = '__all__'


class ElementSalesNetworkUpdateSerializer(serializers.ModelSerializer):
    """
        Сериализатор для экземпляра модели Звено сети продаж.
    """

    class Meta:
        model = ElementSalesNetwork
        fields = '__all__'
        read_only_fields = ['debt']
