import django_filters
from main.models import ElementSalesNetwork


class ElementSalesNetworkFilter(django_filters.rest_framework.FilterSet):
    country = django_filters.CharFilter(field_name="contact__country", lookup_expr="icontains", )

    class Meta:
        model = ElementSalesNetwork
        fields = ("country",)
